package cse455.myphoneapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.CloneNotSupportedException;
import java.lang.Object;
import java.lang.Override;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button buttonCopy = (Button) findViewById(R.id.button);
        buttonCopy.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                EditText editText = (EditText) findViewById(R.id.editText);
                String message = editText.getText().toString();
                TextView textView = (TextView) findViewById(R.id.textView);
                textView.setText(message);
            }
        });
    }
}
